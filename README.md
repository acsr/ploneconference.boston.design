# README #

I started with a folder assets containing general files and master files including rendered graphics exports based on the masters.

* Initially I just share bae files in case to enable others to access all kind of versions.
* This is based on the concept state LY20 from 20160315 by acsr

### How do I get set up? ###

* for now leave everything to acsr
* Configuration
* some fonts
* Vector master files are Adobe Illustrator CS5 and SVG exports

### Contribution guidelines ###

* For now read only

### Intended audience ###

* Website Implementation Team
* Location Design Production
* Merchandise productions
* Social Media icons and campaign banners users

### How to reach out to the designer Armin / acsr ###

Armin Carl Stroß-Radschinski | developer@acsr.de | Twitter: @syncmitter 

Dipl. Designer FH | project-consultant | mobile +49 171 21 94699 | IRC: acsr | Skype: astrossradschinski

--

ACSR industrialdesign | Armin Stroß-Radschinski

Landgrafenstraße 32 · 53842 Troisdorf · Germany

info@acsr.de | http://www.acsr.de | phone +49 2241 946994 · fax +49 2241 946996